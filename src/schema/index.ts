const { gql } = require("apollo-server");

export const typeDefs = gql`
  type User {
    id: ID
    name: String
    lastname: String
    email: String
    gender: String
    avatar: String
  }
  type Query {
    users: [User]
    user(id:ID!): User
  }
  type Mutation {
    saveUser(name:String, lastname:String, email:String!, gender:String, avatar:String): User
  }
`;
