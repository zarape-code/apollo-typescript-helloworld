import { UserAPI } from "../controller/UserAPI";

const userAPI: UserAPI = new UserAPI();
export const resolvers = {
  Query: {
    users: () => userAPI.getUsers(),
    user: (_, { id }) => userAPI.getUser(id),
  },
  Mutation: {
    saveUser: (_, { name, lastname, email, gender, avatar }) =>
      userAPI.saveUser({ name, lastname, email, gender, avatar }),
  },
};
