export class UserAPI {

  users: Array<IUser>;

  constructor() {
    this.users = users;
  }

  getUsers(): Array<IUser> {
    return users
  }
  getUser(id: number): IUser {
    return users.find(user => user.id == id)
  }
  saveUser(user: IUser): IUser {
    return user
  }
  updateUser(id: number): IUser {
    return users.find(user => user.id == id)
  }
  deleteUser(id: number): IUser {
    return users.find(user => user.id == id)
  }
}

interface IUser {
  id?: number
  name: string
  lastname: string
  email: string
  gender: string
  avatar: string
}

const users: Array<IUser> = [
  {
    id: 1,
    name: "Raimund",
    lastname: "Alner",
    email: "ralner0@exblog.jp",
    gender: "Male",
    avatar: "https://robohash.org/etestet.jpg?size=50x50&set=set1",
  },
  {
    id: 2,
    name: "Row",
    lastname: "Diemer",
    email: "rdiemer1@elpais.com",
    gender: "Female",
    avatar: "https://robohash.org/etquidemex.png?size=50x50&set=set1",
  },
  {
    id: 3,
    name: "Zita",
    lastname: "Olivelli",
    email: "zolivelli2@parallels.com",
    gender: "Female",
    avatar: "https://robohash.org/quiaestvoluptates.jpg?size=50x50&set=set1",
  },
];
