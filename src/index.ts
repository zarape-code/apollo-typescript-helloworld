import { typeDefs } from "./schema";
import { resolvers } from "./resolver";
import { ApolloServer } from "apollo-server";

new ApolloServer({ typeDefs, resolvers })
  .listen().then(({ url }) => {
    console.log(`🇲🇽 🚀  Server ready at ${url}`);
  });
