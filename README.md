# GraphQL Apollo Server + TypeScript 

Proyecto que muestra un `Hello World` de [GraphQL](https://graphql.org/) usando [Apollo Server](https://www.apollographql.com/docs/apollo-server/) y [TypeScript](https://www.typescriptlang.org/)

## Ejecutar el proyecto
Modo desarrollo con hotreload en typescript
```sh
$ npm start
```
Compilación para producción
```sh
$ npm run build
```

## Dependencias
Productivas
```sh
$ npm i apollo-server graphql
```
Desarrollo
```sh
$ npm i -D @types/node ts-node typescript
```

## Tecnología empleada
* [GraphQL](https://graphql.org/)
* [Apollo Server](https://www.apollographql.com/docs/apollo-server/) - Implementación de la [GraphQL Spec](http://spec.graphql.org/) en `JS`
* [TypeScript](https://www.typescriptlang.org/) - Tipos para `JS`

## Extensiones de VSCode usadas
* [Apollo GraphQL for VS Code](https://marketplace.visualstudio.com/items?itemName=apollographql.vscode-apollo) - syntax highlighting, intelligent autocomplete, ...

___
### Autor
Creado por [Ethien Salinas](https://www.linkedin.com/in/ethiensalinas/) para zarape.io 🇲🇽